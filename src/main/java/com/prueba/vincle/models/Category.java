package com.prueba.vincle.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@NoArgsConstructor
@Data
@Entity
@Table(name = "category")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;


    @ManyToOne
    @JoinColumn(columnDefinition = "category_type_id", referencedColumnName = "id")
    private CategoryType categoryType;
}
