package com.prueba.vincle.models;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@Data
@Entity
@Table(name = "article")
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column
    private String name;

    @Column
    private Float quantityUnit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "article_package_id")
    private Category articlePackage;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "article_type_id")
    private Category articleType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "article_unit_id")
    private Category articleUnit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "article_location_id")
    private Category articleLocation;
}
