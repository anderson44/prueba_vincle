package com.prueba.vincle.category.controller;


import com.prueba.vincle.category.service.CategoryService;
import com.prueba.vincle.dto.CategoryDTO;
import com.prueba.vincle.models.Category;
import com.prueba.vincle.request.CategoryRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @GetMapping("")
    public Flux<CategoryDTO> findAll() {
        return categoryService.findAll();
    }

    @GetMapping("/{id}")
    public Mono<CategoryDTO> findById(@PathVariable Long id) throws Exception {
        return categoryService.findById(id);
    }

    @PostMapping("")
    public Mono<Category> create(@RequestBody CategoryRequest request) {
        return categoryService.create(request);
    }

    @PatchMapping("/{id}")
    public Mono<Category> update(@PathVariable Long id, @RequestBody CategoryRequest request) throws Exception {
        return categoryService.update(id, request);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        categoryService.deleteById(id);
    }
}
