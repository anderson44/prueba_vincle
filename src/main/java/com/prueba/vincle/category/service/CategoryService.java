package com.prueba.vincle.category.service;


import com.prueba.vincle.category.repository.CategoryJpaRepository;
import com.prueba.vincle.dto.CategoryDTO;
import com.prueba.vincle.mapper.CategoryMapper;
import com.prueba.vincle.models.Category;
import com.prueba.vincle.request.CategoryRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {

    @Autowired
    CategoryJpaRepository categoryJpaRepository;

    public Mono<Category> create(CategoryRequest request) {
        return Mono.justOrEmpty(categoryJpaRepository.save(CategoryMapper.toModel(new Category(), request)));
    }

    public Flux<CategoryDTO> findAll() {
        List<CategoryDTO> list = new ArrayList<>();
        categoryJpaRepository.findAll().stream().forEach(i -> list.add(CategoryMapper.toMap(i)));
        return Flux.fromIterable(list);
    }

    public Mono<CategoryDTO> findById(Long id) throws Exception {
        Category category = categoryJpaRepository.findById(id).orElseThrow(() -> new Exception("Data not found for this id :: " + id));
        return Mono.justOrEmpty(CategoryMapper.toMap(category));
    }

    public Mono<Category> update(Long id, CategoryRequest request) throws Exception {
        Category categoryOld = categoryJpaRepository.findById(id).orElseThrow(() -> new Exception("Data not found for this id :: " + id));
        Category category = categoryJpaRepository.save(CategoryMapper.toModel(categoryOld, request));
        return Mono.justOrEmpty(category);
    }

    public void deleteById(Long id) {
        categoryJpaRepository.deleteById(id);
    }
}

