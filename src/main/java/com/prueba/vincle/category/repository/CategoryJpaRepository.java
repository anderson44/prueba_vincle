package com.prueba.vincle.category.repository;

import com.prueba.vincle.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryJpaRepository extends JpaRepository<Category,Long> {
}
