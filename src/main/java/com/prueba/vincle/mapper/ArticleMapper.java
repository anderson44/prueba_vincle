package com.prueba.vincle.mapper;

import com.prueba.vincle.dto.ArticleDTO;
import com.prueba.vincle.request.ArticleRequest;
import com.prueba.vincle.models.Article;
import com.prueba.vincle.models.Category;
import org.springframework.stereotype.Component;

@Component
public class ArticleMapper {

    public static ArticleDTO toMap(Article a) {
        ArticleDTO articleDTO = new ArticleDTO();
        articleDTO.setId(a.getId());
        articleDTO.setName(a.getName());
        articleDTO.setQuantityUnit(a.getQuantityUnit());
        articleDTO.setArticleType(CategoryMapper.toMap(a.getArticleType()));
        articleDTO.setArticleUnit(CategoryMapper.toMap(a.getArticleUnit()));
        articleDTO.setArticlePackage(CategoryMapper.toMap(a.getArticlePackage()));
        articleDTO.setArticleLocation(CategoryMapper.toMap(a.getArticleLocation()));
        return articleDTO;
    }

    public static Article toModel(Article article, ArticleRequest dto) {
        article.setName(dto.getName());
        article.setQuantityUnit(dto.getQuantityUnit());

        Category articleType = new Category();
        articleType.setId(dto.getArticleTypeId());
        article.setArticleType(articleType);

        Category articlePackage = new Category();
        articlePackage.setId(dto.getArticlePackageId());
        article.setArticlePackage(articlePackage);

        Category articleUnit = new Category();
        articleUnit.setId(dto.getArticleUnitId());
        article.setArticleUnit(articleUnit);

        Category articleLocation = new Category();
        articleLocation.setId(dto.getArticleLocationId());
        article.setArticleLocation(articleLocation);

        return article;
    }
}
