package com.prueba.vincle.mapper;


import com.prueba.vincle.dto.CategoryDTO;
import com.prueba.vincle.dto.CategoryTypeDTO;
import com.prueba.vincle.models.Category;
import com.prueba.vincle.models.CategoryType;
import com.prueba.vincle.request.CategoryRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Objects;

@Component
public class CategoryMapper {
    public static CategoryDTO toMap(Category c) {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(c.getId());
        categoryDTO.setName(c.getName());
        categoryDTO.setCategoryType(CategoryTypeMapper.toMap(c.getCategoryType()));
        return categoryDTO;
    }

    public static Category toModel(Category category, CategoryRequest dto) {

        category.setName(dto.getName());

        CategoryType categoryType = new CategoryType();
        categoryType.setId(dto.getCategoryTypeId());
        category.setCategoryType(categoryType);

        return category;
    }
}
