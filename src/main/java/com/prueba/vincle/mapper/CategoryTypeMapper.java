package com.prueba.vincle.mapper;

import com.prueba.vincle.dto.CategoryTypeDTO;
import com.prueba.vincle.request.CategoryTypeRequest;
import com.prueba.vincle.models.CategoryType;
import org.springframework.stereotype.Component;

@Component
public class CategoryTypeMapper {

    public static CategoryTypeDTO toMap(CategoryType c) {
        CategoryTypeDTO categoryTypeDTO = new CategoryTypeDTO();
        categoryTypeDTO.setId(c.getId());
        categoryTypeDTO.setName(c.getName());
        return categoryTypeDTO;
    }

    public static CategoryType toModel(CategoryType categoryType, CategoryTypeRequest request) {
        categoryType.setName(request.getName());
        return categoryType;
    }
}
