package com.prueba.vincle.categoryType.service;

import com.prueba.vincle.categoryType.repository.CategoryTypeJpaRepository;
import com.prueba.vincle.dto.CategoryTypeDTO;
import com.prueba.vincle.request.CategoryTypeRequest;
import com.prueba.vincle.mapper.CategoryTypeMapper;
import com.prueba.vincle.models.CategoryType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Component
public class CategoryTypeService {

    @Autowired
    CategoryTypeJpaRepository categoryTypeRepository;


    public Mono<CategoryType> create(CategoryTypeRequest request) {
        CategoryType c = categoryTypeRepository.save(CategoryTypeMapper.toModel(new CategoryType(), request));
        return Mono.justOrEmpty(c);
    }

    public Flux<CategoryTypeDTO> findAll() {
        List<CategoryTypeDTO> list = new ArrayList<>();
        for (CategoryType i : categoryTypeRepository.findAll()) {
            list.add(CategoryTypeMapper.toMap(i));
        }
        return Flux.fromIterable(list);
    }

    public Mono<CategoryTypeDTO> findById(Long id) throws Exception {
        CategoryType categoryType = categoryTypeRepository.findById(id).orElseThrow(() -> new Exception("Data not found for this id :: " + id));
        return Mono.justOrEmpty(CategoryTypeMapper.toMap(categoryType));
    }

    public Mono<CategoryType> update(Long id, CategoryTypeRequest request) throws Exception {
        CategoryType categoryTypeOld = categoryTypeRepository.findById(id).orElseThrow(() -> new Exception("Data not found for this id :: " + id));
        CategoryType categoryType = categoryTypeRepository.save(CategoryTypeMapper.toModel(categoryTypeOld, request));
        return Mono.justOrEmpty(categoryType);
    }

    public void deleteById(Long id){
        categoryTypeRepository.deleteById(id);
    }
}
