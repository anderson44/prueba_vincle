package com.prueba.vincle.categoryType.repository;


import com.prueba.vincle.models.CategoryType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryTypeJpaRepository extends JpaRepository<CategoryType, Long> {
}
