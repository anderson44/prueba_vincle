package com.prueba.vincle.categoryType.controller;


import com.prueba.vincle.categoryType.service.CategoryTypeService;
import com.prueba.vincle.dto.CategoryTypeDTO;
import com.prueba.vincle.request.CategoryTypeRequest;
import com.prueba.vincle.models.CategoryType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/categoryType")
public class CategoryTypeController {

    @Autowired
    CategoryTypeService categoryTypeService;

    @GetMapping("")
    public Flux<CategoryTypeDTO> findAll() {
        return categoryTypeService.findAll();
    }

    @GetMapping("/{id}")
    public Mono<CategoryTypeDTO> findById(@PathVariable Long id) throws Exception {
        return categoryTypeService.findById(id);
    }

    @PostMapping("")
    public Mono<CategoryType> create(@RequestBody CategoryTypeRequest request) {
        return categoryTypeService.create(request);
    }

    @PatchMapping("/{id}")
    public Mono<CategoryType> update(@PathVariable Long id, @RequestBody CategoryTypeRequest request) throws Exception {
        return categoryTypeService.update(id, request);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        categoryTypeService.deleteById(id);
    }

}
