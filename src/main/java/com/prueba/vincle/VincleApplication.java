package com.prueba.vincle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class VincleApplication {

	public static void main(String[] args) {
		SpringApplication.run(VincleApplication.class, args);
	}

}
