package com.prueba.vincle.article.repository;

import com.prueba.vincle.models.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface ArticleJpaRepository extends JpaRepository<Article, Long>, JpaSpecificationExecutor<Article> {
}
