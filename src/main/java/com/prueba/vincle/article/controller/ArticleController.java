package com.prueba.vincle.article.controller;

import com.prueba.vincle.article.service.ArticleService;
import com.prueba.vincle.dto.ArticleDTO;
import com.prueba.vincle.request.ArticleRequest;
import com.prueba.vincle.models.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/article")
public class ArticleController {
    @Autowired
    ArticleService articleService;

    @GetMapping("")
    public Flux<ArticleDTO> findAll(@RequestParam("search") String search) {
        return articleService.findAll(search);
    }

    @GetMapping("/{id}")
    public Mono<ArticleDTO> findById(@PathVariable Long id) {
        return articleService.findById(id);
    }

    @PostMapping("")
    public Mono<Article> create(@RequestBody ArticleRequest request) {
        return articleService.create(request);
    }

    @PatchMapping("/{id}")
    public Mono<Article> update(@PathVariable Long id, @RequestBody ArticleRequest request) throws Exception {
        return articleService.update(id, request);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) throws Exception {
        articleService.deleteById(id);
    }
}
