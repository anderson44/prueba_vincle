package com.prueba.vincle.article.service;

import com.prueba.vincle.article.repository.ArticleJpaRepository;
import com.prueba.vincle.dto.ArticleDTO;
import com.prueba.vincle.request.ArticleRequest;
import com.prueba.vincle.mapper.ArticleMapper;
import com.prueba.vincle.models.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ArticleService {

    @Autowired
    ArticleJpaRepository articleJpaRepository;

    public Mono<Article> create(ArticleRequest request) {
        Article s = save(request);
        System.out.println("get id: " + s.getId().toString());
        return Mono.justOrEmpty(s);
    }

    @Transactional
    private Article save(ArticleRequest request) {
        Article article = ArticleMapper.toModel(new Article(), request);
        Article s = articleJpaRepository.save(article);
        return s;
    }

    public Flux<ArticleDTO> findAll(String search) {
        List<ArticleDTO> list = new ArrayList<>();
        articleJpaRepository.findAll(
                (Specification<Article>) (root, query, criteriaBuilder) -> {
                    List<Predicate> predicates = new ArrayList<>();
                    if (StringUtils.hasText(search)) {
                        predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + search.toUpperCase().toLowerCase() + "%"));
                    }
                    Predicate[] predicatesArray = new Predicate[predicates.size()];
                    return criteriaBuilder.and(predicates.toArray(predicatesArray));
                }
        ).stream().forEach(i -> list.add(ArticleMapper.toMap(i)));
        return Flux.fromIterable(list);
    }

    public Mono<ArticleDTO> findById(Long id) {
        Optional<Article> object = articleJpaRepository.findById(id);
        return object.map(article -> Mono.justOrEmpty(ArticleMapper.toMap(article))).orElse(null);
    }

    public Mono<Article> update(Long id, ArticleRequest request) throws Exception {
        Article object = articleJpaRepository.findById(id).orElseThrow(() -> new Exception("Data not found for this id :: " + id));
        Article article = articleJpaRepository.save(ArticleMapper.toModel(object, request));
        return Mono.justOrEmpty(article);
    }

    public void deleteById(Long id) throws Exception {
        Article object = articleJpaRepository.findById(id).orElseThrow(() -> new Exception("Data not found for this id :: " + id));
        articleJpaRepository.deleteById(object.getId());
    }
}
