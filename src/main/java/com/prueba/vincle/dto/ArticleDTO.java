package com.prueba.vincle.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class ArticleDTO {
    private Long id;

    private String name;

    private Float quantityUnit;

    private CategoryDTO articlePackage;

    private CategoryDTO articleType;

    private CategoryDTO articleUnit;

    private CategoryDTO articleLocation;
}
