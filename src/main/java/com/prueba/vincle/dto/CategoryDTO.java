package com.prueba.vincle.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

//import javax.persistence.*;

@NoArgsConstructor
@Data
public class CategoryDTO {

    private Long id;

    private String name;

    private CategoryTypeDTO categoryType;
}
