package com.prueba.vincle.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class ArticleRequest {

    private String name;

    private Float quantityUnit;

    private Long articlePackageId;

    private Long articleTypeId;

    private Long articleUnitId;

    private Long articleLocationId;
}
