package com.prueba.vincle.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CategoryTypeRequest {
    private String name;
}
