package com.prueba.vincle.request;


import lombok.Data;
import lombok.NoArgsConstructor;

//import javax.persistence.*;

@NoArgsConstructor
@Data
public class CategoryRequest {

    private Long id;

    private String name;

    private Long categoryTypeId;
}
