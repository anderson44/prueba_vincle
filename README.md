## Instalación y Ejecución de la Aplicacion
### Pre-requisitos
`Instalar Java-8-JDK`
### Primero descargar depencias vía maven
`mvn dependency:resolve`

### Compilar el jar con el comando
`mvn clean install`

### Ejecutar el comando 
`docker-compose up --build`

#### Una vez ejecutados los comandos, abrir el navegador web y escribir en la barra de dirección http://localhost:8080/swagger-ui/index.htmly verá los endpoint expuestos
