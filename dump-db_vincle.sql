-- public.category_type definition

-- Drop table

-- DROP TABLE public.category_type;

CREATE TABLE public.category_type (
	id serial4 NOT NULL,
	"name" varchar(255) NULL,
	CONSTRAINT category_type_pk PRIMARY KEY (id)
);


-- public.category definition

-- Drop table

-- DROP TABLE public.category;

CREATE TABLE public.category (
	id serial4 NOT NULL,
	"name" varchar(255) NULL,
	parent_id int4 NULL,
	category_type_id int4 NULL,
	CONSTRAINT category_pk PRIMARY KEY (id),
	CONSTRAINT category_fk FOREIGN KEY (category_type_id) REFERENCES public.category_type(id),
	CONSTRAINT category_fk_1 FOREIGN KEY (parent_id) REFERENCES public.category(id)
);


-- public.article definition

-- Drop table

-- DROP TABLE public.article;

CREATE TABLE public.article (
	id serial4 NOT NULL,
	"name" varchar(255) NULL,
	article_type_id int4 NULL,
	article_package_id int4 NULL,
	article_unit_id int4 NULL,
	quantity_unit float4 NULL,
	article_location_id int4 NULL,
	CONSTRAINT article_pk PRIMARY KEY (id),
	CONSTRAINT article_fk FOREIGN KEY (article_type_id) REFERENCES public.category(id),
	CONSTRAINT article_fk_1 FOREIGN KEY (article_package_id) REFERENCES public.category(id),
	CONSTRAINT article_fk_2 FOREIGN KEY (article_unit_id) REFERENCES public.category(id),
	CONSTRAINT article_fk_3 FOREIGN KEY (article_location_id) REFERENCES public.category(id)
);



COPY category_type (id, name) FROM stdin;
1	location
2	type
3	package
4	unit
5	prueba
\.



COPY category (id, name, parent_id, category_type_id) FROM stdin;
1	precisa nevera	\N	1
2	no precisa nevera	\N	1
3	bebidas	\N	2
4	comidas	\N	2
5	salsas	\N	2
6	especies	\N	2
7	caja	\N	3
8	botella	\N	3
9	gramos	\N	4
10	litros	\N	4
\.





COPY article (id, name, article_type_id, article_package_id, article_unit_id, quantity_unit, article_location_id) FROM stdin;
1	arroz	4	7	9	100	2
13	pasta	4	7	9	200	2
18	fidio	4	7	9	110	2
22	caraota	4	7	9	110	2
\.
