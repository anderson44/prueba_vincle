FROM openjdk:8-alpine

ADD target/vincle-0.0.1-SNAPSHOT.jar vincle.jar

#ARG JAR_FILE=*.jar

#COPY ${JAR_FILE} app.jar

ENTRYPOINT ["java","-jar","vincle.jar"]
